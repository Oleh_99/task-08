package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Soil;
import com.epam.rd.java.basic.task8.VisualParameters;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.Lighting;
import com.epam.rd.java.basic.task8.Multiplying;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public void saveToXml(Flowers flowers, String outputXmlFile) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db;
        Document document;
        try {
            db = dbf.newDocumentBuilder();
            document = db.newDocument();

            Element root = document.createElement("flowers");
            document.appendChild(root);
            for (Map.Entry<String, String> flowersAttribute : flowers.getAttributes().entrySet()) {
                root.setAttribute(flowersAttribute.getKey(), flowersAttribute.getValue());
            }

            for (Flower flower : flowers.getFlowers()) {

                Element flowerElement = document.createElement("flower");
                root.appendChild(flowerElement);

                Element nameElement = document.createElement("name");
                nameElement.setTextContent(flower.getName());
                flowerElement.appendChild(nameElement);

                Element soilElement = document.createElement("soil");
                soilElement.setTextContent(flower.getSoil().getTitle());
                flowerElement.appendChild(soilElement);

                Element originElement = document.createElement("origin");
                originElement.setTextContent(flower.getOrigin());
                flowerElement.appendChild(originElement);

                Element visualParametersElement = document.createElement("visualParameters");
                flowerElement.appendChild(visualParametersElement);

                Element stemColourElement = document.createElement("stemColour");
                stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
                visualParametersElement.appendChild(stemColourElement);

                Element leafColourElement = document.createElement("leafColour");
                leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
                visualParametersElement.appendChild(leafColourElement);

                Element aveLenFlowerElement = document.createElement("aveLenFlower");
                aveLenFlowerElement.setAttribute("measure", flower.getVisualParameters().getMeasure());
                aveLenFlowerElement.setTextContent(String.valueOf(flower.getVisualParameters().getAveLenFlower()));
                visualParametersElement.appendChild(aveLenFlowerElement);

                Element growingTipsElement = document.createElement("growingTips");
                flowerElement.appendChild(growingTipsElement);

                Element tempretureElement = document.createElement("tempreture");
                tempretureElement.setAttribute("measure", flower.getGrowingTips().getTemperatureMeasure());
                tempretureElement.setTextContent(String.valueOf(flower.getGrowingTips().getTempreture()));
                growingTipsElement.appendChild(tempretureElement);

                Element lighting = document.createElement("lighting");
                lighting.setAttribute("lightRequiring", flower.getGrowingTips().getLighting().getTitle());
                growingTipsElement.appendChild(lighting);

                Element watering = document.createElement("watering");
                watering.setAttribute("measure", flower.getGrowingTips().getWateringMeasure());
                watering.setTextContent(String.valueOf(flower.getGrowingTips().getWatering()));
                growingTipsElement.appendChild(watering);

                Element multiplying = document.createElement("multiplying");
                multiplying.setTextContent(flower.getMultiplying().toString());
                flowerElement.appendChild(multiplying);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(outputXmlFile));
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    public Flowers getData() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            document = db.parse(xmlFileName);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }

        Flowers flowersRoot = new Flowers();
        if (document == null) {
            throw new NullPointerException("Document, which must contain data from xml file, is empty");
        }
        NodeList flowersNodeList = document.getElementsByTagName("flowers");
        NamedNodeMap attributes = flowersNodeList.item(0).getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            flowersRoot.getAttributes().put(attributes.item(i).getNodeName(), attributes.item(i).getNodeValue());
        }
        flowersNodeList = document.getElementsByTagName("flower");
        List<Flower> flowers = new ArrayList<>();
        for (int i = 0; i < flowersNodeList.getLength(); i++) {
            Node flower = flowersNodeList.item(i);
            Flower tempFlower = new Flower();
            if (parseFlower(flower, tempFlower)) {
                flowers.add(tempFlower);
            }
        }
        flowersRoot.setFlowers(flowers);
        return flowersRoot;
    }

    private boolean parseFlower(Node node, Flower flower) {
        int parameterCounter = 0;
        NodeList nl = node.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                switch (n.getNodeName()) {
                    case "name":
                        flower.setName(getTextData(n));
                        parameterCounter++;
                        break;
                    case "soil":
                        flower.setSoil(Soil.getByValue(getTextData(n)));
                        parameterCounter++;
                        break;
                    case "origin":
                        flower.setOrigin(getTextData(n));
                        parameterCounter++;
                        break;
                    case "visualParameters":
                        if (parseVisualParameters(n, flower)) parameterCounter++;
                        break;
                    case "growingTips":
                        if (parseGrowingTips(n, flower)) parameterCounter++;
                        break;
                    case "multiplying":
                        flower.setMultiplying(Multiplying.getByValue(getTextData(n)));
                        parameterCounter++;
                        break;
                }
            }
        }
        return parameterCounter == 6;
    }

    private boolean parseGrowingTips(Node node, Flower flower) {
        int parameterCounter = 0;
        GrowingTips growingTips = new GrowingTips();
        NodeList nl = node.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                switch (n.getNodeName()) {
                    case "tempreture":
                        growingTips.setTemperatureMeasure(n.getAttributes().getNamedItem("measure").getNodeValue());
                        growingTips.setTempreture(Integer.parseInt(getTextData(n)));
                        parameterCounter++;
                        break;
                    case "lighting":
                        String lightRequiring = n.getAttributes().getNamedItem("lightRequiring").getNodeValue();
                        growingTips.setLighting(Lighting.getByValue(lightRequiring));
                        parameterCounter++;
                        break;
                    case "watering":
                        growingTips.setWatering(Integer.parseInt(getTextData(n)));
                        growingTips.setWateringMeasure(n.getAttributes().getNamedItem("measure").getNodeValue());
                        parameterCounter++;
                        break;
                }
                if (parameterCounter == 3) flower.setGrowingTips(growingTips);
            }
        }
        return parameterCounter == 3;
    }

    private boolean parseVisualParameters(Node nn, Flower flower) {
        int parameterCounter = 0;
        NodeList nl = nn.getChildNodes();
        VisualParameters visualParameters = new VisualParameters();
        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                switch (n.getNodeName()) {
                    case "stemColour":
                        visualParameters.setStemColour(getTextData(n));
                        parameterCounter++;
                        break;
                    case "leafColour":
                        visualParameters.setLeafColour(getTextData(n));
                        parameterCounter++;
                        break;
                    case "aveLenFlower":
                        visualParameters.setAveLenFlower(Integer.parseInt(getTextData(n)));
                        visualParameters.setMeasure(n.getAttributes().getNamedItem("measure").getNodeValue());
                        parameterCounter++;
                        break;
                }
                if (parameterCounter == 3) flower.setVisualParameters(visualParameters);
            }
        }
        return parameterCounter == 3;
    }

    private String getTextData(Node n) {
        return n.getFirstChild().getNodeValue().replace("\n", "").trim();
    }
}
