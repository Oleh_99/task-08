package com.epam.rd.java.basic.task8;

import java.util.Comparator;

public class Sorter {
    public static final Comparator<Flower> SORT_FLOWERS_BY_AVE_LEN_FLOWER = (o1, o2) -> {
        Integer flower1 = o1.getVisualParameters().getAveLenFlower();
        Integer flower2 = o2.getVisualParameters().getAveLenFlower();
        return flower1.compareTo(flower2);
    };
    public static final Comparator<Flower> SORT_FLOWERS_BY_NAME = (o1, o2) -> {
        String flower1 = o1.getName();
        String flower2 = o2.getName();
        return flower1.compareTo(flower2);
    };
    public static final Comparator<Flower> SORT_FLOWERS_BY_ORIGIN = (o1, o2) -> {
        String flower1 = o1.getOrigin();
        String flower2 = o2.getOrigin();
        return flower1.compareTo(flower2);
    };

    public static void sortFlowersByOrigin(final Flowers flowers) {
        flowers.getFlowers().sort(SORT_FLOWERS_BY_ORIGIN);
    }

    public static void sortFlowersByName(final Flowers flowers) {
        flowers.getFlowers().sort(SORT_FLOWERS_BY_NAME);
    }

    public static void sortFlowersByAveLenFlower(final Flowers flowers) {
        flowers.getFlowers().sort(SORT_FLOWERS_BY_AVE_LEN_FLOWER);
    }
}
