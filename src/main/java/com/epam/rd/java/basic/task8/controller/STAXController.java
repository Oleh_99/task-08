package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.VisualParameters;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.Lighting;
import com.epam.rd.java.basic.task8.Soil;
import com.epam.rd.java.basic.task8.Multiplying;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }
    // PLACE YOUR CODE HERE
    private static String formatXML(String xml) throws TransformerException {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));

        return output.toString();

    }

    public Flowers loadData() throws IOException, XMLStreamException {
        Flowers flowers = new Flowers();
        Flower flower = new Flower();
        VisualParameters visualParameters = new VisualParameters();
        GrowingTips growingTips = new GrowingTips();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                Attribute attribute;
                switch (startElement.getName().getLocalPart()) {
                    case "flowers":
                        flowers.setAttributes(new LinkedHashMap<>());
                        flowers.setFlowers(new ArrayList<>());
                        Iterator<Namespace> namespaceIterator = startElement.getNamespaces();
                        while (namespaceIterator.hasNext()) {
                            Namespace namespace = namespaceIterator.next();
                            String key = namespace.getName().getPrefix();
                            if (!namespace.getName().getLocalPart().isBlank()) {
                                key = key + ":" + namespace.getName().getLocalPart();
                            }
                            flowers.getAttributes().put(key, namespace.getValue());
                        }
                        Iterator<Attribute> attributeIterator = startElement.getAttributes();
                        while (attributeIterator.hasNext()) {
                            attribute = attributeIterator.next();
                            String key = attribute.getName().getPrefix();
                            if (!attribute.getName().getLocalPart().isBlank()) {
                                key = key + ":" + attribute.getName().getLocalPart();
                            }
                            flowers.getAttributes().put(key, attribute.getValue());
                        }
                        break;
                    case "flower":
                        flower = new Flower();
                        break;
                    case "name":
                        nextEvent = reader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case "soil":
                        nextEvent = reader.nextEvent();
                        flower.setSoil(Soil.getByValue(nextEvent.asCharacters().getData()));
                        break;
                    case "origin":
                        nextEvent = reader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case "visualParameters":
                        visualParameters = new VisualParameters();
                        break;
                    case "stemColour":
                        nextEvent = reader.nextEvent();
                        visualParameters.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case "leafColour":
                        nextEvent = reader.nextEvent();
                        visualParameters.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case "aveLenFlower":
                        attribute = startElement.getAttributeByName(QName.valueOf("measure"));
                        visualParameters.setMeasure(attribute.getValue());
                        nextEvent = reader.nextEvent();
                        visualParameters.setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "growingTips":
                        growingTips = new GrowingTips();
                        break;
                    case "tempreture":
                        attribute = startElement.getAttributeByName(
                                new QName("measure"));
                        growingTips.setTemperatureMeasure(attribute.getValue());
                        nextEvent = reader.nextEvent();
                        growingTips.setTempreture(
                                Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "lighting":
                        attribute = startElement.getAttributeByName(QName.valueOf("lightRequiring"));
                        growingTips.setLighting(Lighting.getByValue(attribute.getValue()));
                        break;
                    case "watering":
                        attribute = startElement.getAttributeByName(QName.valueOf("measure"));
                        growingTips.setWateringMeasure(attribute.getValue());
                        nextEvent = reader.nextEvent();
                        growingTips.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "multiplying":
                        nextEvent = reader.nextEvent();
                        flower.setMultiplying(Multiplying.getByValue(nextEvent.asCharacters().getData()));
                        break;
                }
            } else if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                switch (endElement.getName().getLocalPart()) {
                    case "growingTips":
                        flower.setGrowingTips(growingTips);
                        break;
                    case "visualParameters":
                        flower.setVisualParameters(visualParameters);
                        break;
                    case "flower":
                        flowers.getFlowers().add(flower);
                        break;
                    case "flowers":
                        break;
                }
            }
        }
        return flowers;
    }

    public void saveToXml(Flowers flowers, String outputXmlFileName) throws IOException, XMLStreamException, TransformerException {
        XMLOutputFactory output = XMLOutputFactory.newInstance();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XMLStreamWriter writer = output.createXMLStreamWriter(out);

        writer.writeStartDocument();
        writer.writeStartElement("flowers");
        for (Map.Entry<String, String> attribute : flowers.getAttributes().entrySet()) {
            writer.writeAttribute(attribute.getKey(), attribute.getValue());
        }

        for (Flower flower : flowers.getFlowers()) {
            writer.writeStartElement("flower");
            writer.writeStartElement("name");
            writer.writeCharacters(flower.getName());
            writer.writeEndElement();

            writer.writeStartElement("soil");
            writer.writeCharacters(flower.getSoil().getTitle());
            writer.writeEndElement();

            writer.writeStartElement("origin");
            writer.writeCharacters(flower.getOrigin());
            writer.writeEndElement();

            writer.writeStartElement("visualParameters");
            writer.writeStartElement("stemColour");
            writer.writeCharacters(flower.getVisualParameters().getStemColour());
            writer.writeEndElement();

            writer.writeStartElement("leafColour");
            writer.writeCharacters(flower.getVisualParameters().getLeafColour());
            writer.writeEndElement();

            writer.writeStartElement("aveLenFlower");
            writer.writeAttribute("measure", flower.getVisualParameters().getMeasure());
            writer.writeCharacters(String.valueOf(flower.getVisualParameters().getAveLenFlower()));
            writer.writeEndElement();

            writer.writeEndElement();

            writer.writeStartElement("growingTips");
            writer.writeStartElement("tempreture");
            writer.writeAttribute("measure", flower.getGrowingTips().getTemperatureMeasure());
            writer.writeCharacters(String.valueOf(flower.getGrowingTips().getTempreture()));
            writer.writeEndElement();

            writer.writeStartElement("lighting");
            writer.writeAttribute("lightRequiring", flower.getGrowingTips().getLighting().getTitle());
            writer.writeEndElement();

            writer.writeStartElement("watering");
            writer.writeAttribute("measure", flower.getGrowingTips().getWateringMeasure());
            writer.writeCharacters(String.valueOf(flower.getGrowingTips().getWatering()));
            writer.writeEndElement();

            writer.writeEndElement();

            writer.writeStartElement("multiplying");
            writer.writeCharacters(flower.getMultiplying().getTitle());
            writer.writeEndElement();

            writer.writeEndElement();
        }

        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();

        String xml = out.toString(StandardCharsets.UTF_8);
        String transformedXML = formatXML(xml);
        Files.writeString(Path.of(new File(outputXmlFileName).getName()), transformedXML, StandardCharsets.UTF_8);
    }
}