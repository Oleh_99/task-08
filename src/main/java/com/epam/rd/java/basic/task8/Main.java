package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        Flowers flowers = domController.getData();
        // sort (case 1)
        Sorter.sortFlowersByName(flowers);
        // save
        String outputXmlFile = "output.dom.xml";
        domController.saveToXml(flowers, outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        flowers = saxController.getData();
        // sort  (case 2)
        // PLACE YOUR CODE HERE
        Sorter.sortFlowersByAveLenFlower(flowers);
        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE
        saxController.writeToXml(flowers, outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        flowers = staxController.loadData();
        // sort  (case 3)
        // PLACE YOUR CODE HERE
        Sorter.sortFlowersByOrigin(flowers);
        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        staxController.saveToXml(flowers, outputXmlFile);
    }
}
