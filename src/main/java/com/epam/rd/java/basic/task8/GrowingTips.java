package com.epam.rd.java.basic.task8;

public class GrowingTips {
    private int tempreture;
    private String temperatureMeasure;
    private Lighting lighting;
    private int watering;
    private String wateringMeasure;

    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public void setTemperatureMeasure(String temperatureMeasure) {
        this.temperatureMeasure = temperatureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", temperatureMeasure='" + temperatureMeasure + '\'' +
                ", lighting=" + lighting +
                ", watering=" + watering +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                '}';
    }
}
