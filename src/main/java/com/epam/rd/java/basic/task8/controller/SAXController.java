package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.VisualParameters;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.Lighting;
import com.epam.rd.java.basic.task8.Soil;
import com.epam.rd.java.basic.task8.Multiplying;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;
    private Flowers flowers;
    private Flower flower;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private StringBuilder elementData;


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    @Override
    public void characters(char[] ch, int start, int length) {
        if (elementData != null) {
            elementData.append(ch, start, length);
        }
    }

    public void writeToXml(Flowers flowers, String outputXmlFile) throws IOException, XMLStreamException, TransformerException {
        STAXController staxController = new STAXController(xmlFileName);
        staxController.saveToXml(flowers, outputXmlFile);
    }

    public Flowers getData() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        SAXController saxHandler = this;
        saxParser.parse(new File(xmlFileName), saxHandler);

        return flowers;
    }


    @Override
    public void startDocument() {
        flowers = new Flowers();
    }

    @Override
    public void startElement(String uri, String lName, String qName, Attributes attr) {
        switch (qName) {
            case "flowers":
                for (int i = 0; i < attr.getLength(); i++) {
                    flowers.getAttributes().put(attr.getLocalName(i), attr.getValue(i));
                }
                break;
            case "flower":
                flower = new Flower();
                break;
            case "name":
            case "soil":
            case "origin":
            case "stemColour":
            case "leafColour":
            case "multiplying":
                elementData = new StringBuilder();
                break;
            case "visualParameters":
                visualParameters = new VisualParameters();
                break;
            case "aveLenFlower":
                elementData = new StringBuilder();
                visualParameters.setMeasure(attr.getValue("measure"));
                break;
            case "growingTips":
                growingTips = new GrowingTips();
                break;
            case "tempreture":
                elementData = new StringBuilder();
                growingTips.setTemperatureMeasure(attr.getValue("measure"));
                break;
            case "lighting":
                elementData = new StringBuilder();
                growingTips.setLighting(Lighting.getByValue(attr.getValue("lightRequiring")));
                break;
            case "watering":
                elementData = new StringBuilder();
                growingTips.setWateringMeasure(attr.getValue("measure"));
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "flower":
                flowers.getFlowers().add(flower);
                break;
            case "name":
                flower.setName(elementData.toString());
                break;
            case "soil":
                flower.setSoil(Soil.getByValue(elementData.toString()));
                break;
            case "origin":
                flower.setOrigin(elementData.toString());
                break;
            case "visualParameters":
                flower.setVisualParameters(visualParameters);
                break;
            case "stemColour":
                visualParameters.setStemColour(elementData.toString());
                break;
            case "leafColour":
                visualParameters.setLeafColour(elementData.toString());
                break;
            case "aveLenFlower":
                visualParameters.setAveLenFlower(Integer.parseInt(elementData.toString()));
                break;
            case "growingTips":
                flower.setGrowingTips(growingTips);
                break;
            case "tempreture":
                growingTips.setTempreture(Integer.parseInt(elementData.toString()));
                break;
            case "lighting":
                break;
            case "watering":
                growingTips.setWatering(Integer.parseInt(elementData.toString()));
                break;
            case "multiplying":
                flower.setMultiplying(Multiplying.getByValue(elementData.toString()));
                break;
        }
    }
}