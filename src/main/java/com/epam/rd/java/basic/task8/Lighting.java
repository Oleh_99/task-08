package com.epam.rd.java.basic.task8;

import java.util.Arrays;

public enum Lighting {
    YES("yes"),
    NO("no");

    private final String title;

    Lighting(String title) {
        this.title = title;
    }

    static public Lighting getByValue(String value) {
        return Arrays.stream(Lighting.values())
                .filter(soil -> soil.getTitle().equals(value))
                .findFirst().orElseThrow();
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
