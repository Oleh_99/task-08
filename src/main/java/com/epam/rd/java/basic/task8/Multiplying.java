package com.epam.rd.java.basic.task8;

import java.util.Arrays;

public enum Multiplying {
    LEAVES("листья"),
    CUTTINGS("черенки"),
    SEEDS("семена");

    private final String title;

    Multiplying(String title) {
        this.title = title;
    }

    static public Multiplying getByValue(String value) {
        return Arrays.stream(Multiplying.values())
                .filter(soil -> soil.getTitle().equals(value))
                .findFirst().orElseThrow();
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
